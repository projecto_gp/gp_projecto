const assert = require('chai').assert;
//const User = require('../Models/user');
var firebase = require('firebase');
const config = require("../routes/config.json");

firebase.initializeApp(config);
const auth = firebase.auth();

describe('Criar Utilizador', function () {
    it('Deverá criar utilizador com sucesso', async function () {
        //let kk=await criaUser(932546758);
        var phone = retornaUser(await criaUser(932546758));
        assert.equal(phone, 932546758, 'Utilizador não foi criado com sucesso !!!')
    })
    //const ide = await criaUser(932546758);
    //var phone = retornaUser(ide);

    //});
});

async function criaUser(tlmv) {
    name = 'Nome Teste';
    email = 'kkcoisa@kkcoisa.com';
    bornDate = new Date(2001, 01, 01);
    address = 'Rua XPTO';
    phoneNumber = tlmv;
    password = 'password';
    confpassword = 'password';

    await auth.createUserWithEmailAndPassword(email, password).then(function () {
        var user = firebase.auth().currentUser;
        user.updateProfile({
            displayName: name,
            email: email,
            phoneNumber: phoneNumber
        }).then(function () {
            writeUserData(user.uid, name, email, bornDate, address, phoneNumber);


        }, function (error) {
            return error.message;
        });
        return user.uid;
    }, function (error) {
        return error.message;
    });
}

function retornaUser(uid) {
    try {
        var pn = "";
        var validaUser = firebase.database().ref('users').child(uid);
        validaUser.once('value').then(function (snapshot) {
            var childdata = snapshot.val();
            pn = childdata.phoneNumber;
        }).then(() => {
            return pn;
        });
    } catch (error) {
        return -1;
    }
}

function writeUserData(userId, name, email, bornDate, address, phoneNumber) {
    firebase.database().ref('users/' + userId).set({
        username: name,
        email: email,
        bornDate: bornDate,
        address: address,
        phoneNumber: phoneNumber,
        role: "Utilizador",
        isActive: true
    });
}
var express = require("express");
var app = express();
const fs = require('fs');
var http = require('http').Server(app);
var io = require('socket.io')(http);
const session = require('express-session');

var user = require('./routes/user');
var auth = require('./routes/auth');
var animal = require('./routes/animal');
var adoption = require('./routes/adoption');
var notif = require('./routes/notif');
var donation = require('./routes/donation');

var path = require('path');
app.use(express.static('public'));
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.disable('etag');

app.set('views', [__dirname + '/public/views/users', __dirname + '/public/views/animals', __dirname + '/public/views/adoptions', __dirname + '/public/views/notifications', __dirname + '/public/views/donations']);
app.set('view engine', 'ejs');

app.get('/healthz', (req, res) => {
    res.status(200).send('OK');
})

app.use(session({
    secret: 'kkcoisaserve',
    resave: false,
    saveUninitialized: false,
}));
app.use('/auth', auth);
app.use('/user', user);
app.use('/animal', animal);
app.use('/adoption', adoption);
app.use('/notif', notif);
app.use('/donation', donation);
app.use('/', auth);
app.io = io;


var server = app.listen(8080, function () {
    var host = server.address().address === "::" ? "localhost" :
        server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
});

module.exports = app;
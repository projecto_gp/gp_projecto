var firebase = require('firebase');
const config = require("../routes/config.json");

var request = require('supertest')
    , app = require('../app');

describe('/GET Registar', () => {
    it('deverá mostrar a página registar utilizador', (done) => {
        request(app).get("/register")
            .expect(200)
            .expect(/Efetue o seu registo/, done)

    });
});
describe('/POST Registar', function () {
    it('Deverá criar utilizador com sucesso', (done) => {
        request(app).post("/register")
            .send({
                name: 'Nome Teste',
                email: 'kkcoisa@kkcoisa.com',
                bornDate: new Date(2001, 01, 01),
                address: 'Rua XPTO',
                phoneNumber: '932546758',
                password: 'password',
                confpassword: 'password'
            })
            .expect(200)
            .expect(/Nome Teste/,done)
            /*.end(function (err, res) {

                console.log("Erro - "+err);
                console.log("\nResposta - "+res.text);


            });*/
    });
});

describe('FAIL /POST Registar', function () {
    it('Deverá indicar que  utilizador já existe', (done) => {
        request(app).post("/register")
            .send({
                name: 'Nome Teste',
                email: 'kkcoisa@kkcoisa.com',
                bornDate: new Date(2001, 01, 01),
                address: 'Rua XPTO',
                phoneNumber: '932546758',
                password: 'password',
                confpassword: 'password'
            })
            .expect(200)
            .expect(/O email já existe/, done)
        /*.end(function (err, res) {
            console.log(res.text);
        });*/
    });
});



describe('/GET Mostrar Perfil', () => {
    it('deverá mostrar a página perfil utilizador', (done) => {
        request(app).get("/user/profile")
            .expect(200)
            .expect(/Perfil/, done)

    });
});
describe('/GET Editar Perfil', () => {
    it('deverá mostrar a página editar perfil utilizador', (done) => {
        request(app).get("/user/editProfile")
            .expect(200)
            .expect(/Editar Perfil/, done)

    });
});

describe('/POST Editar Perfil', function () {
    it('Deverá editar o perfil do utilizador com sucesso', (done) => {
        request(app).post("/user/editProfile")
            .send({
                name: 'Nome Teste Alterado',
                bornDate: new Date(2001, 01, 02),
                email: 'kkcoisa@kkcoisa.com',
                address: 'Rua Alterada',
                phoneNumber: '932546758'
            })
            .expect(200)
            .expect(/Nome Teste Alterado/, done)
            
    });
});

describe('/GET Remover Conta', () => {
    it('deverá mostrar a página para desactivar conta', (done) => {
        request(app).get("/user/removeAccount")
            .expect(200)
            .expect(/Tem a certeza/, done)

    });
});

describe('/POST Remover Conta', function () {
    it('Deverá remover a conta do utilizador com sucesso', (done) => {
        request(app).post("/user/removeAccountConfirm")
            .expect(200)
            .expect(/Autenticar/, done)
            
    });
});


describe('/GET login', () => {
    it('deverá mostrar a página para efectuar o login', (done) => {
        request(app).get("/auth/login")
            .expect(200)
            .expect(/Efetue a autenticação/, done)

    });
});

describe('FAIL /POST Login', function () {
    it('Deverá indicar que o login falhou', (done) => {
        request(app).post("/auth/login")
            .send({
                email: 'asdfgty@gmail.com',
                password: 'kkcoisa'
            })
            .expect(200)
            .expect(/O nome de utilizador ou a palavra-passe estão incorretos!/, done)
            
    });
});

describe('/POST Login', function () {
    it('Deverá efectuar o login com sucesso', (done) => {
        request(app).post("/auth/login")
            .send({
                email: 'testemundial14@gmail.com',
                password: 'memesmemes1'
            })
            .expect(200)
            .expect(/Administrador/, done)
            
    });
});


describe('/GET Listar Utilizadores', () => {
    it('deverá listar utilizadores', (done) => {
        request(app).get("/user/listUsers")
            .expect(200)
            .expect(/Lista de Contas/, done)

    });
});






describe('/GET logout', () => {
    it('deverá efectuar o logout', (done) => {
        request(app).get("/auth/logout")
            .expect(200)
            .expect(/Autenticar/, done)

    });
});




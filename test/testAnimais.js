var firebase = require('firebase');
const config = require("../routes/config.json");

var request = require('supertest'),
    app = require('../app');


describe('/GET Listar Animais User Anonimo', () => {
    it('deverá listar animais', (done) => {
        request(app).get("/animal/listAnimalsHomePage")
            .expect(200)
            .expect(/Lista de Animais/, done)
    });
});

describe('/GET login', () => {
    it('deverá mostrar a página para efectuar o login', (done) => {
        request(app).get("/auth/login")
            .expect(200)
            .expect(/Efetue a autenticação/, done)

    });
});

describe('/POST Login', function () {
    it('Deverá efectuar o login com sucesso', (done) => {
        request(app).post("/auth/login")
            .send({
                email: 'testemundial14@gmail.com',
                password: 'memesmemes1'
            })
            .expect(200)
            .expect(/Administrador/, done)

    });
});

describe('/GET Listar Animais User Autenticado', () => {
    it('deverá listar animais', (done) => {
        request(app).get("/animal/listAnimals")
            /*.send({
                kk: '',
                gg: ''
            })*/
            .expect(200)
            .expect(/Inserir novo animal/)
            .expect(/Lista de Animais/, done)
        /*.end(function (err, res) {
            console.log(res.text);
        });*/

    });
});

describe('/GET Inserir Animal', () => {
    it('deverá mostrar a página que permite inserir animais', (done) => {
        request(app).get("/animal/insertAnimal")
            .expect(200)
            .expect(/Criar ficha de animal/, done)
    });
});

describe('/POST Inserir Animal', () => {
    it('deverá inserir animal com sucesso', (done) => {
        //let data=new Date(2010,01,01).toDateString();
        request(app).post("/animal/insertAnimal")
            .field('name', 'NomeCao')
            .field('weight', '20')
            .field('breed', 'Caniche')
            .field('size', 'Médio')
            .field('sex', 'Macho')
            .field('bornDate', new Date(2010, 01, 01).toDateString())
            .attach('imageAnimalUpload', './test/imagemCao.jpg')
            .expect(200)
            .expect(/Inserir novo animal/, done)
        /*.end(function (err, res) {
            console.log(res.text);
        });*/
    });
});




describe('/GET logout', () => {
    it('deverá efectuar o logout', (done) => {
        request(app).get("/auth/logout")
            .expect(200)
            .expect(/Autenticar/, done)

    });
});
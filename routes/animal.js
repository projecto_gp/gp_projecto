var express = require("express");
var router = express.Router();
var firebase = require('firebase');

var fs = require('fs');
var buffer = require('buffer');
var path = require('path');


const fileUpload = require('express-fileupload');
const uuidv4 = require('uuid/v4');

router.use(fileUpload());

function getRoleOfCurrentUser(user) {
    var role;
    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
        role = snapshot.val().role;
    });
    return role;
}

async function getNextAnimal() {
    var animalNames = [];
    var aux = [];
    var adoptions = firebase.database().ref('donations').orderByChild('donationType').equalTo("3");
    await adoptions.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            if (childdata.assigned == false) {
                let input = {};
                input.donationId = childSnapshot.key;
                input.animalName = childdata.animalName;
                animalNames.push(input);
            }
        });
    }).then(() => {
        aux = animalNames[0];
    });
    return aux;
}

router.get('/insertAnimal', function (req, res) {
    var resultado = [];
    var helpRoot = firebase.database().ref("/help/insertAnimalHelp");
    var user = firebase.auth().currentUser;
    var img = "/images/avatar.png";
    getNextAnimal().then(aux => {

        animalName = (aux) ? (aux.animalName) : "";
        helpRoot.once('value').then(function (snapshot) {
            let input = {};
            input.name = snapshot.val().insertAnimalName;
            input.weight = snapshot.val().insertAnimalWeight;
            input.breed = snapshot.val().insertAnimalBreed;
            input.size = snapshot.val().insertAnimalSize;
            input.sex = snapshot.val().insertAnimalSex;
            input.bornDate = snapshot.val().insertAnimalBornDate;
            input.photo = snapshot.val().insertAnimalPhoto;
            resultado.push(input);
        }).then(() => {
            firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
                role = snapshot.val().role;
            }).then(() => {
                res.render('insertAnimal.ejs', {
                    data: JSON.stringify(resultado),
                    name: user.displayName,
                    animalName: animalName,
                    photo: img,
                    maxData: new Date().toISOString().substring(0, 10),
                    role: getRoleOfCurrentUser(user)
                });
            });
        });
    });
});

router.post('/insertAnimal', function (req, res) {
    var user = firebase.auth().currentUser;
    var resultado = [];
    var util = firebase.database().ref('animals').orderByChild("wasAdopted").equalTo(false);
    var role;
    var sampleFile = req.files.imageAnimalUpload;
    const base64String = sampleFile.data.toString('base64');

    getNextAnimal().then((aux) => {

        animalName = (aux) ? (aux.animalName) : "";
        donationId = (aux) ? (aux.donationId) : "";
        var json = {
            assigned: true
        }

        name = (animalName) ? animalName : req.body.name;
        weight = req.body.weight;
        breed = req.body.breed;
        size = req.body.size;
        sex = req.body.sex
        bornDate = req.body.bornDate;
        photo = base64String;

        writeAnimalData(name, weight, breed, size, sex, bornDate, photo);
        updateDonationData(donationId, json);

        util.once('value').then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childdata = childSnapshot.val();
                let input = {};
                if (childdata.isActive) {
                    input.id = childSnapshot.key;
                    input.Nome = childdata.name;
                    input.Photo = (childdata.photo) ? ("data:image/png;base64," + childdata.photo) : "/images/avatar.png";
                    resultado.push(input);
                }
            });
        }).then(() => {
            firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
                role = snapshot.val().role;
            }).then(() => {
                res.render('listAnimals.ejs', {
                    data: JSON.stringify(resultado),
                    name: user.displayName,
                    role: role
                });
            });
        });
    });
});

function writeAnimalData(name, weight, breed, size, sex, bornDate, photo) {
    var animalRef = firebase.database().ref('animals');
    var newAnimalRef = animalRef.push();
    newAnimalRef.set({
        name: name,
        weight: weight,
        breed: breed,
        size: size,
        sex: sex,
        bornDate: bornDate,
        photo: photo,
        isActive: true,
        wasAdopted: false,
        raca_porte: breed + '_' + size,
    });
}

router.get('/listAnimals', async function (req, res) {
    if (req.session.userObj != null && req.session.userObj != undefined) {
        console.log(req.session.userObj.uid);

    }
    var user = firebase.auth().currentUser;
    var resultado = [];
    var util = firebase.database().ref('animals').orderByChild("wasAdopted").equalTo(false);
    var role;
    await util.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            let input = {};
            if (childdata.isActive) {
                input.id = childSnapshot.key;
                input.Nome = childdata.name;
                input.Photo = (childdata.photo) ? ("data:image/png;base64," + childdata.photo) : "/images/avatar.png";
                resultado.push(input);
            }
        });
    })
    if (user != undefined && user != null) {
        await firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
            role = snapshot.val().role;
        })
        res.render('listAnimals.ejs', {
            role: role,
            data: JSON.stringify(resultado),
            name: user.displayName
        });
    } else {
        res.render('listAnimalsHomepage.ejs', {
            data: JSON.stringify(resultado)
        });
    }
});

router.get('/listAnimalsOrderedFiltered', function (req, res) {
    var user = firebase.auth().currentUser;
    var order = req.query.order;
    var filter = req.query.filter;
    var resultado = [];
    var util = firebase.database().ref('animals');
    if (filter === "") {
        util = util.orderByChild(normalizaPesquisa(order));

    } else {
        switch (filter.split("_")[0]) {
            case ("raca"):
                util = util.orderByChild('breed').equalTo(filter.split("_")[1]);
                break;
            case ("Porte"):
                util = util.orderByChild('size').equalTo(filter.split("_")[1]);
                break;
        }
    }
    util.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            let input = {};
            input.Id = childSnapshot.key;
            input.Nome = childdata.name;
            input.Idade = childdata.bornDate;
            input.Photo = (childdata.photo) ? ("data:image/png;base64," + childdata.photo) : "/images/avatar.png";
            input.Raça = childdata.breed;
            if (!childdata.wasAdopted && childdata.isActive)
                resultado.push(input);
        });
    }).then(() => {
        if (filter.split("_")[0] === "Idade") {
            resultado = filtraIdade(resultado, filter.split("_")[1])
        }
        if (filter != "" || order != "") {
            switch (order) {
                case ("Nome"):
                    resultado.sort((a, b) => a.Nome.toLowerCase().localeCompare(b.Nome.toLowerCase()));
                    break;
                case ("Raça"):
                    resultado.sort((a, b) => a.Raça.toLowerCase().localeCompare(b.Raça.toLowerCase()));
                    break;
                case ("Data"):
                    resultado.sort((a, b) => new Date(a.Idade) - new Date(b.Idade));
                    break;
            }
        }

        var resp = "";
        if (resultado.length > 0) {
            var ejs = require('ejs');
            var path = require('path');
            if (user == undefined && user == null) {
                ejs.renderFile(path.join(__dirname, '../public/views/animals/listOfAnimalsHomepage.ejs'), {
                    data: resultado
                }, function (err, html) {
                    resp = html;
                });
            } else {
                ejs.renderFile(path.join(__dirname, '../public/views/animals/listOfAnimals.ejs'), {
                    data: resultado
                }, function (err, html) {
                    resp = html;
                });
            }
        } else {
            resp = "<p style=\"color:white\">A pesquisa não retornou qualquer resultado</p>"
        }
        res.send(resp);
    });

})


router.get('/animalDetails/:id', function (req, res) {
    var user = firebase.auth().currentUser;
    var resultado = [];
    var animalId = req.params.id;
    var role;
    var adoptions = firebase.database().ref('adoptions').orderByChild('animalId').equalTo(animalId);
    var users = firebase.database().ref('users');
    var currAdopt = {
        userId: null,
        hostType: null,
        username: null
    };

    //var adop = adoptions.orderByChild('animalId').equalTo(animalId);

    adoptions.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            currAdopt.userId = childdata.userId;
            currAdopt.hostType = childdata.hostType;
            var users_adoptions = users.orderByKey().equalTo(childdata.userId);
            users_adoptions.once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childdata = childSnapshot.val();
                    currAdopt.username = childdata.username;
                });
            });
        });

        firebase.database().ref('/animals/' + animalId).once('value').then(function (snapshot) {
            let input = {};
            input.id = animalId;
            input.nameAnimal = (snapshot.val() && snapshot.val().name) || '999999999';
            input.weight = snapshot.val().weight;
            input.breed = snapshot.val().breed;
            input.size = snapshot.val().size;
            input.sex = snapshot.val().sex;
            input.age = calcAge(snapshot.val().bornDate);
            input.godfather = (currAdopt.hostType === "Apadrinhamento") ? currAdopt.username : "nenhum";
            input.photo = ("data:image/png;base64," + snapshot.val() && "data:image/png;base64," + snapshot.val().photo) || "/images/avatar.png";
            resultado.push(input);
        });
    });
    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
        role = snapshot.val().role;
    }).then(() => {
        setTimeout(() => {
            res.render('animalDetails.ejs', {
                data: JSON.stringify(resultado),
                name: user.displayName,
                role: role
            });
        }, 2000);
    });
});

router.get('/animalDetailsHomepage/:id', function (req, res) {
    var resultado = [];
    var animalId = req.params.id;

    firebase.database().ref('/animals/' + animalId).once('value').then(function (snapshot) {
        let input = {};
        input.id = animalId;
        input.nameAnimal = (snapshot.val() && snapshot.val().name) || '999999999';
        input.weight = snapshot.val().weight;
        input.breed = snapshot.val().breed;
        input.size = snapshot.val().size;
        input.sex = snapshot.val().sex;
        input.age = calcAge(snapshot.val().bornDate);
        input.photo = ("data:image/png;base64," + snapshot.val() && "data:image/png;base64," + snapshot.val().photo) || "/images/avatar.png";
        resultado.push(input);
    }).then(() => {

        res.render('animalDetailsHomepage.ejs', {
            data: JSON.stringify(resultado)
        });

    });
});

router.get('/listAnimalsHomepage', function (req, res) {
    var resultado = [];
    var util = firebase.database().ref('animals').orderByChild("wasAdopted").equalTo(false);
    var role;
    var user = firebase.auth().currentUser;
    util.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            let input = {};
            if (childdata.isActive) {
            input.Id = childSnapshot.key;
            console.log(childSnapshot.key)
            input.Nome = childdata.name;
            input.Photo = (childdata.photo) ? ("data:image/png;base64," + childdata.photo) : "/images/avatar.png";
            resultado.push(input);
            }
        });
    }).then(() => {

        res.render('listAnimalsHomepage.ejs', {
            data: JSON.stringify(resultado)
        });

    });
});


router.get("/editAnimal/:id", function (req, res) {
    var user = firebase.auth().currentUser;
    var animalId = req.params.id;
    var role;
    firebase.database().ref('/animals/' + animalId).once('value').then(function (snapshot) {
        var nameAnimal = (snapshot.val() && snapshot.val().name) || 'tobias';
        var weight = (snapshot.val() && snapshot.val().weight) || 0;
        var breed = (snapshot.val() && snapshot.val().breed) || 'rafeiro';
        var size = (snapshot.val() && snapshot.val().size) || 'small';
        var sex = snapshot.val().sex;
        var bornDate = (snapshot.val() && snapshot.val().bornDate) || 0;
        var photo = ("data:image/png;base64," + snapshot.val() && "data:image/png;base64," + snapshot.val().photo) || "/images/avatar.png";

        firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
            role = snapshot.val().role;
        }).then(() => {
            res.render('editAnimal.ejs', {
                role: role,
                id: animalId,
                nameAnimal: nameAnimal,
                weight: weight,
                breed: breed,
                size: size,
                sex: sex,
                bornDate: bornDate,
                photo: photo,
                maxData: new Date().toISOString().substring(0, 10),
                name: user.displayName
            });
        });
    });
});


router.post('/editAnimal/:id', function (req, res) {
    var user = firebase.auth().currentUser;
    var animalId = req.params.id;

    name = req.body.name,
        weight = req.body.weight,
        breed = req.body.breed,
        size = req.body.size,
        sex = req.body.sex,
        bornDate = req.body.bornDate

    var sampleFile;
    var base64String;

    var json = {
        name: name,
        weight: weight,
        breed: breed,
        size: size,
        sex: sex,
        bornDate: bornDate,
    };

    if (req.files != null) {
        sampleFile = req.files.imageAnimalUpload;
        base64String = sampleFile.data.toString('base64');
        json.photo = base64String
    }
    updateAnimalData(animalId, json);
    res.redirect('/animal/listAnimals');

});

router.get('/removeAnimal/:id', function (req, res) {
    var user = firebase.auth().currentUser;
    var role;
    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
        role = snapshot.val().role;
    }).then(() => {
        res.render('removeAnimal.ejs', {
            animalId: req.params.id,
            name: user.displayName,
            role: role
        });
    });
});

router.get('/removeAnimalConfirm/:id', function (req, res) {
    var user = firebase.auth().currentUser;
    var animalId = req.params.id;
    var json = {
        isActive: false,
    };
    updateAnimalData(animalId, json);
    res.redirect('/animal/listAnimals');

});




function calcAge(dateString) {
    var today = new Date();
    var DOB = new Date(dateString);
    var totalMonths = (today.getFullYear() - DOB.getFullYear()) * 12 + today.getMonth() - DOB.getMonth();
    totalMonths += today.getDay() < DOB.getDay() ? -1 : 0;
    var years = today.getFullYear() - DOB.getFullYear();
    if (DOB.getMonth() > today.getMonth())
        years = years - 1;
    else if (DOB.getMonth() === today.getMonth())
        if (DOB.getDate() > today.getDate())
            years = years - 1;

    var days;
    var months;

    if (DOB.getDate() > today.getDate()) {
        months = (totalMonths % 12);
        if (months == 0)
            months = 11;
        var x = today.getMonth();
        switch (x) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12: {
                var a = DOB.getDate() - today.getDate();
                days = 31 - a;
                break;
            }
            default: {
                var a = DOB.getDate() - today.getDate();
                days = 30 - a;
                break;
            }
        }

    }
    else {
        days = today.getDate() - DOB.getDate();
        if (DOB.getMonth() === today.getMonth())
            months = (totalMonths % 12);
        else
            months = (totalMonths % 12) + 1;
    }
    var age = years + ' anos ' + months + ' meses ' + days + ' dias';
    return age;
}
 
  

function updateAnimalData(animalId, json) {
    firebase.database().ref('animals/' + animalId).update(json);
}

function updateDonationData(donationId, json) {
    firebase.database().ref('donations/' + donationId).update(json);
}

function normalizaPesquisa(param) {
    var resposta = "";
    switch (param) {
        case "Nome":
            resposta = "name";
            break;
        case "Raça":
            resposta = "breed";
            break;
        case "Data":
            resposta = "bornDate";
            break;
        default:
            resposta = "";
    }
    return resposta;
}

function filtraIdade(lista, filtro) {
    var resultado = [];
    lista.forEach(element => {
        var birthdate = new Date(element.Idade);
        var cur = new Date();
        var diff = cur - birthdate;
        var age = Math.floor(diff / 31557600000);

        if (age > 6 && filtro.includes('7')) {
            resultado.push(element);
        } else {
            if ((3 < age && age < 7) && filtro.includes('6')) {
                resultado.push(element);
            } else {
                if (age <= 3 && filtro.includes('3')) {
                    resultado.push(element);
                }
            }
        }
    });

    return resultado;
}
router.get('/suggestion/:id', function (req, res) {
    var idcurAnimal = req.params.id;
    var suggest = req.query.raca_porte;
    var resultado = [];
    var util = firebase.database().ref('animals');
    util = util.orderByChild('raca_porte').equalTo(suggest);
    util.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            let input = {};
            if (idcurAnimal != childSnapshot.key && childdata.isActive) {
                input.Id = childSnapshot.key;
                input.Nome = childdata.name;
                input.Photo = (childdata.photo) ? ("data:image/png;base64," + childdata.photo) : "/images/avatar.png";
                resultado.push(input);
            }

        });
    }).then(() => {
        var resp = "";
        if (resultado.length > 0) {
            var ejs = require('ejs');
            var path = require('path');
            ejs.renderFile(path.join(__dirname, '../public/views/animals/suggestion.ejs'), {
                data: resultado
            }, function (err, html) {
                resp = html;
            });
        } else {
            //resp = "<p style=\"color:white\">A pesquisa não retornou qualquer resultado</p>"
        }
        res.send(resp);

    });

});

module.exports = router;
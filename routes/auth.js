var express = require("express");
var router = express.Router();
var firebase = require('firebase');
const config = require("./config.json");

firebase.initializeApp(config);
const auth = firebase.auth();


const fileUpload = require('express-fileupload');
const uuidv4 = require('uuid/v4');

router.use(fileUpload());



router.get("/homepage", function (req, res) {
    res.redirect('/animal/listAnimals');
});

router.get("/homepageAuth", function (req, res) {
    var donations = firebase.database().ref('donations').orderByChild('value');
    
    var user = req.session.userObj;
    var role = req.session.role;
    var userId, nameDonor, userPhoto, value;
    var hasDonations = false;
    donations.once('value').then(function (snapshot) {
        if (snapshot.exists()) {
            snapshot.forEach(function (childSnapshot) {
               //console.log(childSnapshot.val())
                userId = childSnapshot.val().userId;
                value = childSnapshot.val().value;
                console.log(value)
            });
            hasDonations = true;
            firebase.database().ref('/users/' + userId).once('value').then(function (snapshot) {
                nameDonor = snapshot.val().username;
                userPhoto = (snapshot.val().photo) ? ("data:image/png;base64," + snapshot.val().photo) : "/images/avatar.png";
            }).then(() => {
                res.render('authenticatedHomepage.ejs', {
                    name: user.displayName,
                    role: role,
                    username: nameDonor,
                    userPhoto: userPhoto,
                    hasDonations: hasDonations,
                    value: value
                });
            })
        }else{
            res.render('authenticatedHomepage.ejs', {
                name: user.displayName,
                role: role,
                hasDonations: hasDonations
            });
        } 
    })
});

router.get("/register", function (req, res) {
    var resultado = [];
    var helpRoot = firebase.database().ref("/help/registerHelp");
    helpRoot.once('value').then(function (snapshot) {
        let input = {};
        input.name = snapshot.val().registerName;
        input.address = snapshot.val().registerAddress;
        input.bornDate = snapshot.val().registerBornDate;
        input.email = snapshot.val().registerEmail;
        input.phoneNumber = snapshot.val().registerPhoneNumber;
        input.password = snapshot.val().registerPassword;
        input.confPassword = snapshot.val().registerConfPassword
        resultado.push(input);
    }).then(() => {
        return res.render('register.ejs', {
            data: JSON.stringify(resultado)
        });
    });
});

router.get("/login", function (req, res) {
    var resultado = [];
    var helpRoot = firebase.database().ref('/help/loginHelp');
    helpRoot.once('value').then(function (snapshot) {
        let input = {};
        input.email = snapshot.val().email;
        input.password = snapshot.val().password;
        resultado.push(input);
    }).then(() => {
        return res.render('login.ejs', {
            data: JSON.stringify(resultado)
        });
    });
});

router.get("/logout", function (req, res) {
    firebase.auth().signOut().then(function () {
        res.redirect('/animal/listAnimalsHomepage');
        req.session.destroy();
    }).catch(function (error) {
    })
});
router.get("/", function (req, res) {
    res.redirect('/animal/listAnimalsHomepage');
});

router.post('/register', async function (req, res) {
    var role;
    var user;
    var resultado = [];
    var helpRoot = firebase.database().ref("/help/registerHelp");
    name = req.body.name;
    email = req.body.email;
    bornDate = req.body.bornDate;
    address = req.body.address;
    phoneNumber = req.body.phoneNumber;
    password = req.body.password;
    confpassword = req.body.confpassword;

    const promise = await auth.createUserWithEmailAndPassword(email, password).then(async function () {
        user = firebase.auth().currentUser;
        await user.updateProfile({
            displayName: name,
            email: email,
            phoneNumber: phoneNumber
        }).then(function () {

        }, function (error) {
        });
        req.session.userObj = user;
        writeUserData(user.uid, name, email, bornDate, address, phoneNumber);
        firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
            role = snapshot.val().role;
            req.session.role = role;
        }).then(() => {
            res.redirect('/auth/homepageAuth');
        });
    }).catch(function (err) {
        helpRoot.once('value').then(function (snapshot) {
            let input = {};
            input.name = snapshot.val().registerName;
            input.address = snapshot.val().registerAddress;
            input.bornDate = snapshot.val().registerBornDate;
            input.email = snapshot.val().registerEmail;
            input.phoneNumber = snapshot.val().registerPhoneNumber;
            input.password = snapshot.val().registerPassword;
            input.confPassword = snapshot.val().registerConfPassword
            resultado.push(input);
        }).then(() => {
            return res.render('register.ejs', {
                data: JSON.stringify(resultado),
                erro: "O email já existe!"
            });
        });
    });


});

router.post('/login', function (req, res) {
    email = req.body.email;
    password = req.body.password;


    const promise = auth.signInWithEmailAndPassword(email, password).then(function () {

        var user = firebase.auth().currentUser;
        try {
            req.session.userObj = user;
        } catch (error) {
        }

        var userId = req.session.userObj.uid;
        var role;
        firebase.database().ref('/users/' + userId).once('value').then(function (snapshot) {
            role = snapshot.val().role;
            req.session.role = role;
        }).then(() => {
            res.redirect('/auth/homepageAuth');
            
        });
    }).catch(function (error) {
        var resultado = [];
        var helpRoot = firebase.database().ref('/help/loginHelp');
        helpRoot.once('value').then(function (snapshot) {
            let input = {};
            input.email = snapshot.val().email;
            input.password = snapshot.val().password;
            resultado.push(input);
        }).then(() => {
            res.render('login.ejs', {
                data: JSON.stringify(resultado),
                erro: "O nome de utilizador ou a palavra-passe estão incorretos!"
            });
        });
    });
    promise.catch(e => console.log(e.message));
});

firebase.auth().onAuthStateChanged(firebaseUser => {
    if (firebaseUser) {
    } else { }
});

function writeUserData(userId, name, email, bornDate, address, phoneNumber) {
    firebase.database().ref('users/' + userId).set({
        username: name,
        email: email,
        bornDate: bornDate,
        address: address,
        phoneNumber: phoneNumber,
        role: "Utilizador",
        isActive: true
    });
    let noti = firebase.database().ref('Notifications').child(userId);
    noti.set({
        role: 'Utilizador',
        full: false

    });
    let mensagem = firebase.database().ref('Notifications/' + userId + '/messages').push();
    mensagem.set({
        title: 'Bem vindo',
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque interdum rutrum sodales. Nullam mattis fermentum libero, non volutpat.",
        read: false
    });
}

module.exports = router;
var express = require("express");
var router = express.Router();
var firebase = require('firebase');
const uuidv4 = require('uuid/v4')

router.get('/listNotifications', async function (req, res) {
    var resultado = [];
    var user = req.session.userObj;
    var adoptionId;
    var employee = user;
    var notificationId, messageId;
    var util = firebase.database().ref('Notifications/' + user.uid + '/messages');
    await util.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            let input = {};
            input.id = childSnapshot.key;
            input.Corpo = childdata.body;
            input.Lida = (childdata.notifType != "Adoção" && childdata.notifType != "FAT") ? true : childdata.read;
            input.Titulo = childdata.title;
            input.AdoptionId = childdata.adoptionId;
            input.NotifType = childdata.notifType;
            input.Data = childdata.date;
            resultado.push(input);

            if (childdata.notifType != "Adoção" && childdata.notifType != "FAT") {
                adoptionId = childdata.adoptionId;
                var ref = firebase.database().ref("Notifications");

                ref.once("value", function (snapshot) {
                    snapshot.forEach(function (snapshot) {
                        if (snapshot.key == employee.uid) {
                            notificationId = snapshot.key;
                            snapshot.child("messages").forEach(function (messageSnapshot) {
                                if (messageSnapshot.val().adoptionId == adoptionId) {
                                    messageId = messageSnapshot.key;
                                }
                            });
                        }
                    });
                }).then(() => {
                    notiJson = {
                        read: true
                    }
                    updateNotification(messageId, notificationId, notiJson);
                });
            }
        });
    });



    res.render('listNotifications', {
        role: req.session.role,
        data: JSON.stringify(resultado),
        name: user.displayName
    });
})

// router.get('/countNot', async function (req, res) {
//     let conta = 0;
//     var user = /*firebase.auth().currentUser*/ req.session.userObj;
//     var util = firebase.database().ref('Notifications/' + user.uid + '/messages');
//     await util.once('value').then(function (snapshot) {
//         conta = snapshot.numChildren();

//     });
//     res.send('' + conta);
// })

router.get('/countNot', async function (req, res) {
    let conta = 0;
    var user = /*firebase.auth().currentUser*/ req.session.userObj;
    var util = firebase.database().ref('Notifications/' + user.uid + '/messages').orderByChild("read").equalTo(false);
    await util.once('value').then(function (snapshot) {
        conta = snapshot.numChildren();
    });
    res.send('' + conta);
})

router.get('/populaPrimeiraVez', async function (req, res) {
    var util = firebase.database().ref('users');
    await util.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            let notifi = firebase.database().ref('Notifications');
            let not = notifi.child(childSnapshot.key);
            not.set({
                adoptionId: "",
                full: false,
                userRole: childdata.role,
            })
            let msg = firebase.database().ref('Notifications/' + childSnapshot.key + '/messages');
            let mensagem = msg.push();
            var today = new Date();
            var date = today.getFullYear() + '-0' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;
            dateTime
            mensagem.set({
                title: 'Bem vindo',
                body: "Obrigado pelo seu registo! Dê uma vista de olhos pelos animais disponiveis... :)",
                notifType: "Welcome",
                date: dateTime,
                read: false
            })
        })
    })
    console.log('done');
})


function updateNotification(messageId, notificationId, json) {
    firebase.database().ref('Notifications/' + notificationId + "/messages/" + messageId).update(json);
}

module.exports = router;
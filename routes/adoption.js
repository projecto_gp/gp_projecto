var express = require("express");
var router = express.Router();
var firebase = require('firebase');

router.get('/adoptionRequest/:id', function (req, res) {

    var animalId = req.params.id;
    var role;
    var user = firebase.auth().currentUser;
    var requestDate = new Date().toISOString().substring(0, 10);
    var hostType = "Adoção";


    var adoptionId = writeAdoptionData(user.uid, animalId, hostType, requestDate, "");
    alertStaff(hostType, adoptionId);


    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
        role = snapshot.val().role;
    }).then(() => {
        res.render('adoptionSuccess.ejs', {
            id: req.params.id,
            name: user.displayName,
            role: role,
            pedido: "Adoção"
        });
    });

});


router.get("/removeAdoptionConfirm/:id", function (req, res) {
    var adoptionId = req.params.id;
    var adoption = firebase.database().ref('/adoptions/' + adoptionId);
    adoption.remove();
    res.redirect('/adoption/listAdoptions');
});


router.get('/removeAdoption/:id', function (req, res) {
    var user = firebase.auth().currentUser;
    var role;
    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
        role = snapshot.val().role;
    }).then(() => {
        res.render('removeAdoption.ejs', {
            adoptionId: req.params.id,
            name: user.displayName,
            role: role
        });
    });
});

router.get('/removeAdoptionConfirm/:id', function (req, res) {
    var adoptionId = req.params.id;
    var adoption = firebase.database().ref('/adoptions/' + adoptionId);
    adoption.remove();
    res.redirect('/adoption/listAdoptions');
});

router.get("/adoptionDetails/:id", function (req, res) {
    var username, phoneNumber, address, userPhoto, role, email;
    var animalName, breed, size, gender, animalPhoto;
    var adoptionType, endDate;
    var adoptionId = req.params.id;
    var user = firebase.auth().currentUser;
    var userId;
    var animalId;


    firebase.database().ref('/adoptions/' + adoptionId).once('value').then(function (snapshot) {
        userId = snapshot.val().userId;
        animalId = snapshot.val().animalId;
        adoptionType = snapshot.val().hostType;
        endDate = snapshot.val().endDate;
    }).then(() => {
        firebase.database().ref('/users/' + userId).once('value').then(function (snapshot) {
            username = snapshot.val().username;
            email = snapshot.val().email;
            phoneNumber = snapshot.val().phoneNumber;
            address = snapshot.val().address;
            userPhoto = (snapshot.val().photo) ? ("data:image/png;base64," + snapshot.val().photo) : "/images/avatar.png";
        }).then(() => {
            firebase.database().ref('/animals/' + animalId).once('value').then(function (snapshot) {
                animalName = snapshot.val().name;
                breed = snapshot.val().breed;
                size = snapshot.val().size;
                gender = snapshot.val().sex;
                animalPhoto = ("data:image/png;base64," + snapshot.val() && "data:image/png;base64," + snapshot.val().photo) || "/images/avatar.png";
            }).then(() => {
                firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
                    role = snapshot.val().role;
                }).then(() => {
                    res.render('adoptionDetails.ejs', {
                        username: username,
                        email: email,
                        phoneNumber: phoneNumber,
                        address: address,
                        userPhoto: userPhoto,
                        role: role,
                        animalName: animalName,
                        breed: breed,
                        size: size,
                        gender: gender,
                        animalPhoto: animalPhoto,
                        name: user.displayName,
                        adoptionType: adoptionType,
                        adoptionId: adoptionId,
                        endDate: endDate
                    });
                });
            });
        });
    });
});
router.post("/adoptionDetailsConfirm/:id", function (req, res) {
    var adoptionId = req.params.id;
    var animalId;
    var userId, userRole;
    var json = {
        wasAccepted: true,
        acceptanceDate: new Date().toISOString().substring(0, 10)
    };

    var animalJson = undefined;
    firebase.database().ref('/adoptions/' + adoptionId).once('value').then(function (snapshot) {
        animalId = snapshot.val().animalId;
        if (snapshot.val().hostType === "Adoção" || snapshot.val().hostType === "FAT") {
            animalJson = {
                wasAdopted: true
            };
            userId = snapshot.val().userId;
        } else {
            animalJson = {
                wasAdopted: false
            };
        }
    }).then(function () {
        let notifi = firebase.database().ref('Notifications');
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;
        let msg = firebase.database().ref('Notifications/' + userId + '/messages');
        let mensagem = msg.push();
        mensagem.set({
            title: 'Confirmação',
            body: "O seu pedido foi aceite, pode dirigir-se ao canil!",
            notifType: "Confirmação",
            date: dateTime,
            read: false
        });

        updateAnimalnData(animalId, animalJson);
        updateAdoptionData(adoptionId, json);
        var adoptions = firebase.database().ref('adoptions').orderByChild('wasAccepted').equalTo(false);
        var result = [];
        adoptions.once('value').then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childdata = childSnapshot.val();
                if (childdata.animalId == animalId) {
                    let interested = { userId: null };
                    interested.userId = childdata.userId;
                    result.push(interested);
                }                
            });
        }).then(() => {
            for (value in result) {
                alertInterested(result[value].userId);
            }
        }).then(() => {
            res.redirect('/notif/listNotifications');
        });
    });
});


router.get("/patronAnimal/:id", function (req, res) {
    var user = firebase.auth().currentUser;
    var role;
    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
        role = snapshot.val().role;
    }).then(() => {
        res.render('patronAnimal.ejs', {
            id: req.params.id,
            name: user.displayName,
            role: role,
            minData: new Date().toISOString().substring(0, 10)
        });
    });

});

router.get("/adoptionSheet/:id", function (req, res) {
    var adoptionId = req.params.id;
    var username, phoneNumber, address, userPhoto, role, email;
    var animalName, breed, size, gender, animalPhoto;
    var user = firebase.auth().currentUser;
    var userId, animalId, adoptionType, endDate;
    var employee = user;
    var notificationId, messageId;
    var ref = firebase.database().ref("Notifications");

    firebase.database().ref('/adoptions/' + adoptionId).once('value').then(function (snapshot) {
        userId = snapshot.val().userId;
        animalId = snapshot.val().animalId;
        adoptionType = snapshot.val().hostType;
        endDate = snapshot.val().endDate;
    });

    ref.once("value", function (snapshot) {
        snapshot.forEach(function (snapshot) {
            if (snapshot.key == employee.uid) {
                notificationId = snapshot.key;
                snapshot.child("messages").forEach(function (messageSnapshot) {
                    if (messageSnapshot.val().adoptionId == adoptionId) {
                        messageId = messageSnapshot.key;
                    }
                });
            }
        });
    }).then(() => {

        firebase.database().ref('/users/' + userId).once('value').then(function (snapshot) {
            username = snapshot.val().username;
            phoneNumber = snapshot.val().phoneNumber;
            address = snapshot.val().address;
            email = snapshot.val().email;
            userPhoto = (snapshot.val().photo) ? ("data:image/png;base64," + snapshot.val().photo) : "/images/avatar.png";
            role = snapshot.val().role;
        }).then(() => {
            firebase.database().ref('/animals/' + animalId).once('value').then(function (snapshot) {
                animalName = snapshot.val().name;
                breed = snapshot.val().breed;
                size = snapshot.val().size;
                gender = snapshot.val().sex;
                animalPhoto = ("data:image/png;base64," + snapshot.val() && "data:image/png;base64," + snapshot.val().photo) || "/images/avatar.png";
            }).then(() => {
                notiJson = {
                    read: true
                }
                updateNotification(messageId, notificationId, notiJson);
                res.render('adoptionConfirm.ejs', {
                    username: username,
                    phoneNumber: phoneNumber,
                    address: address,
                    email: email,
                    userPhoto: userPhoto,
                    role: role,
                    animalName: animalName,
                    breed: breed,
                    size: size,
                    gender: gender,
                    animalPhoto: animalPhoto,
                    name: user.displayName,
                    adoptionType: adoptionType,
                    adoptionId: adoptionId,
                    endDate: endDate
                });
            });
        });
    });

});

router.post('/patronAnimal/:id', function (req, res) {
    var user = firebase.auth().currentUser;
    var role;
    var animalId = req.params.id;
    var hostType = "Apadrinhamento";
    var requestDate = new Date().toISOString().substring(0, 10);
    var endDate = req.body.endDate;
    writeAdoptionData(user.uid, animalId, hostType, requestDate, endDate);
    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
        role = snapshot.val().role;
    }).then(() => {
        res.render('patronAnimalSuccess.ejs', {
            id: req.params.id,
            name: user.displayName,
            role: role,
            pedido: "Apadrinhamento"
        });
    });
});

router.get("/patronAnimalConfirm/:id", function (req, res) {
    var adoptionId = req.params.id;
    var animalId;
    var json = {
        wasAccepted: true,
        acceptanceDate: new Date().toISOString().substring(0, 10)
    };

    updateAdoptionData(adoptionId, json)
    res.redirect('/adoption/listAdoptions');
});

router.get("/temporaryHost/:id", function (req, res) {
    var user = firebase.auth().currentUser;
    var role;
    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
        role = snapshot.val().role;
    }).then(() => {
        res.render('temporaryHost.ejs', {
            id: req.params.id,
            name: user.displayName,
            role: role,
            minData: new Date().toISOString().substring(0, 10)
        });
    });
});

router.post('/temporaryHost/:id', function (req, res) {
    var user = firebase.auth().currentUser;
    var role;
    var animalId = req.params.id;
    var hostType = "FAT";
    var requestDate = new Date().toISOString().substring(0, 10);
    var endDate = req.body.endDate;
    var adoptionId = writeAdoptionData(user.uid, animalId, hostType, requestDate, endDate);
    alertStaff(hostType, adoptionId);
    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
        role = snapshot.val().role;
    }).then(() => {
        res.render('adoptionSuccess.ejs', {
            id: req.params.id,
            name: user.displayName,
            role: role,
            pedido: "Acolhimento temporário"
        });
    });
});

router.get("/listAdoptions", function (req, res) {
    var result = [];
    var user = firebase.auth().currentUser;
    var role;
    var adoptions = firebase.database().ref('adoptions').orderByChild('wasAccepted').equalTo(true);
    var users = firebase.database().ref('users');
    var animals = firebase.database().ref('animals');
    adoptions.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            var adopt = {
                adopt: childSnapshot,
                hostType: childdata.hostType,
                animalName: null,
                adopterName: null,
                adopterId: null,
                adoptionId: childSnapshot.key,
                hostType: childdata.hostType
            };

            var users_adoptions = users.orderByKey().equalTo(childdata.userId);
            users_adoptions.once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childdata = childSnapshot.val();
                    adopt.adopterName = childdata.username;
                    adopt.adopterId = childSnapshot.key;
                });
            });
            var animals_adoptions = animals.orderByKey().equalTo(childdata.animalId);
            animals_adoptions.once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childdata = childSnapshot.val();
                    adopt.animalName = childdata.name;
                });
            });
            result.push(adopt);
        });
        firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
            role = snapshot.val().role;
        }).then(() => {
            setTimeout(() => {
                res.render('listAdoptions.ejs', {
                    data: JSON.stringify(result),
                    name: user.displayName,
                    role: role
                });
            }, 2000);
        });
    });
});

function writeAdoptionData(userId, animalId, hostType, requestDate, endDate) {
    var adoptionRef = firebase.database().ref('adoptions');
    var newAdoptionRef = adoptionRef.push();
    newAdoptionRef.set({
        userId: userId,
        animalId: animalId,
        requestDate: requestDate,
        hostType: hostType,
        wasAccepted: false,
        endDate: endDate
    });
    return newAdoptionRef.key;
}

function updateAdoptionData(adoptionId, json) {
    firebase.database().ref('adoptions/' + adoptionId).update(json);
}

function updateAnimalnData(animalId, json) {
    firebase.database().ref('animals/' + animalId).update(json);
}

function updateNotification(messageId, notificationId, json) {
    firebase.database().ref('Notifications/' + notificationId + "/messages/" + messageId).update(json);
}

function getRoleOfCurrentUser(user) {
    var role;
    var aux;
    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
        role = snapshot.val().role;
    }).then(()=>{
        aux = role;
    });
    return aux;
   
}

router.get("/testeAlerta", function (req, res) {
    alertStaff();
});

async function alertStaff(hostType, adoptionId) {
    var body;
    if (hostType == "Adoção") {
        body = "Foi criada uma nova adopção , por favor dê seguimento ao processo !!!"
    } else {
        body = "Foi criada um novo pedido de acolhimento temporário , por favor dê seguimento ao processo !!!";
    }
    let util = firebase.database().ref('Notifications').orderByChild('userRole').equalTo('Funcionário');
    await util.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            let notifi = firebase.database().ref('Notifications/' + childSnapshot.key + '/messages');
            let message = notifi.push();
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;
            message.set({
                adoptionId: adoptionId,
                title: 'Nova ' + hostType,
                body: body,
                notifType: hostType,
                date: dateTime,
                read: false
            });

        });
    });
}

async function alertInterested(userId) {
    let util = firebase.database().ref('Notifications').orderByKey().equalTo(userId);
    await util.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            let notifi = firebase.database().ref('Notifications/' + childSnapshot.key + '/messages');
            let message = notifi.push();
            message.set({
                title: 'Info',
                body: "Lamentamos mas o animal que pretende já não se encontra disponivel :(",
                notifType: "Info",
                date: new Date().toISOString().substring(0, 10),
                read: false
            });

        });
    });
}



module.exports = router;
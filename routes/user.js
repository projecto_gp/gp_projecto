var express = require("express");
var router = express.Router();
var firebase = require('firebase');

var fs = require('fs');
var buffer = require('buffer');
var path = require('path');


const fileUpload = require('express-fileupload');
const uuidv4 = require('uuid/v4');

router.use(fileUpload());


router.post('/editProfile', function (req, res) {

    var user = firebase.auth().currentUser;
    var role = req.session.role;
    username = req.body.name;
    email = req.body.email;
    bornDate = req.body.bornDate;
    address = req.body.address;
    phoneNumber = req.body.phoneNumber;
    newPassword = req.body.password;

    var sampleFile;
    var base64String;
    if (req.files == null) {
        base64String = "";
    } else {
        sampleFile = req.files.imageUpload;
        base64String = sampleFile.data.toString('base64');
    }

    user.updateProfile({
        displayName: username,
        email: email
    }).then(function () {}).catch(function (error) {});

    user.updateEmail(email).then(() => {}, (error) => {});
    var json = {
        username: username,
        email: email,
        bornDate: bornDate,
        address: address,
        phoneNumber: phoneNumber,
        photo: base64String
    };
    updateUserData(user.uid, json);

    res.redirect('/auth/homepageAuth');

});


router.get('/removeAccount', function (req, res) {
    var user = req.session.userObj;
    var role = req.session.role;
    res.render('removeAccount.ejs', {
        name: user.displayName,
        role: role
    });

});

router.post('/removeAccountConfirm', function (req, res) {
    var user = firebase.auth().currentUser;
    user.delete().then(function () {
        var json = {
            isActive: false,
        };
        updateUserData(user.uid, json);
        res.redirect('/animal/listAnimalsHomepage');
    }, function (error) {
        // An error happened.
    });

});

router.get("/editProfile", function (req, res) {
    var userId = /*firebase.auth().currentUser.uid*/ req.session.userObj.uid;
    firebase.database().ref('/users/' + userId).once('value').then(function (snapshot) {
        var address = (snapshot.val() && snapshot.val().address) || 'cidade anónima';
        var bornDate = (snapshot.val() && snapshot.val().bornDate) || 'Anonymous';
        var email = (snapshot.val() && snapshot.val().email) || 'xpto@hotmail.com';
        var phoneNumber = (snapshot.val() && snapshot.val().phoneNumber) || '999999999';
        var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
        var photo = snapshot.val().photo;
        var img = (photo) ? ("data:image/png;base64," + photo) : "/images/avatar.png";
        var role = snapshot.val().role;
        res.render('editProfile.ejs', {
            address: address,
            bornDate: bornDate,
            email: email,
            phoneNumber: phoneNumber,
            name: username,
            photo: img,
            role: role
        });
    });
});

router.get("/profile", function (req, res) {
    var userId = req.session.userObj.uid;
    firebase.database().ref('/users/' + userId).once('value').then(function (snapshot) {
        var address = (snapshot.val() && snapshot.val().address) || 'cidade anónima';
        var bornDate = (snapshot.val() && snapshot.val().bornDate) || '10101997';
        var email = (snapshot.val() && snapshot.val().email) || 'xpto@hotmail.com';
        var phoneNumber = (snapshot.val() && snapshot.val().phoneNumber) || '999999999';
        var name = (snapshot.val() && snapshot.val().username) || 'Anonymous';
        var photo = snapshot.val().photo;
        var img = (photo) ? ("data:image/png;base64," + photo) : "/images/avatar.png";
        var role = (snapshot.val() && snapshot.val().role) || 'Sem cargo';

        var age = calcAge(bornDate);

        res.render('profile.ejs', {
            address: address,
            bornDate: bornDate,
            email: email,
            phoneNumber: phoneNumber,
            name: name,
            age: age,
            photo: img,
            role: role
        });
    });
});

router.post('/changePassword', function (req, res) {
    var user = firebase.auth().currentUser;
    var role;
    newPassword = req.body.password;
    oldPassword = req.body.oldPassword;
    var credential = firebase.auth.EmailAuthProvider.credential(user.email, oldPassword);
    var resultado = [];
    var helpRoot = firebase.database().ref("/help/changePassword");

    helpRoot.once('value').then(function (snapshot) {
        role = snapshot.val().role;
        let input = {};
        input.currPassword = snapshot.val().currPassword;
        input.newPassword = snapshot.val().newPassword;
        input.newPasswordConfirm = snapshot.val().newPasswordConfirm;
        resultado.push(input);
    });

    user.reauthenticateAndRetrieveDataWithCredential(credential).then(function () {
        user.updatePassword(newPassword).then(() => {}, (error) => {});

        firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
            role = snapshot.val().role;
        }).then(() => {
            res.redirect('/auth/homepageAuth');
        });

    }).catch(function (error) {
        res.render('changePassword.ejs', {
            name: user.displayName,
            role: role,
            erro: "A palavra-passe não corresponde!",
            data: JSON.stringify(resultado)
        });

    });

});


router.get("/recoverPassword", function (req, res) {
    res.render('recoverPassword.ejs');
});

router.post("/recoverPassword", function (req, res) {
    var auth = firebase.auth();
    auth.useDeviceLanguage();
    auth.sendPasswordResetEmail(req.body.email).then(function () {
        res.send(JSON.stringify('enviado'));
    }).catch(function (error) {
        if (error.code == 'auth/invalid-email' || error.code == 'auth/user-not-found') {
            res.render('recoverPassword.ejs', {
                erro: "O email não existe!"
            });
        } else {
            res.render('recoverPassword.ejs', {
                erro: "O email não existe!"
            });
        }
    });
});


router.get("/recoverPasswordSuccess", function (req, res) {
    res.render('recoverPasswordSuccess.ejs');
});

router.get("/editPassword", function (req, res) {
    var user = /*firebase.auth().currentUser*/ req.session.userObj;
    var resultado = [];
    var helpRoot = firebase.database().ref('/help/editPasswordHelp');
    var role;
    helpRoot.once('value').then(function (snapshot) {
        role = snapshot.val().role;
        let input = {};
        input.newPassword = snapshot.val().newPassword;
        input.newPasswordConfirm = snapshot.val().newPasswordConfirm;
        resultado.push(input);
    }).then(() => {
        return res.render('editPassword.ejs', {
            data: JSON.stringify(resultado),
            name: user.displayName
        });
    });
});


router.get("/listUsers", async function (req, res) {
    var user = /*firebase.auth().currentUser*/ req.session.userObj;
    var resultado = [];
    var util = firebase.database().ref('users');
    var role = req.session.role;
    util.once('value').then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childdata = childSnapshot.val();
                let input = {};
                input.id = childSnapshot.key;
                input.name = childdata.username;
                input.role = childdata.role;
                input.isActive = childdata.isActive;
                input.photo = (childdata.photo) ? ("data:image/png;base64," + childdata.photo) : "/images/avatar.png";
                resultado.push(input);
            });
        })
        .then(() => {
            /*
                    firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {
                        role = snapshot.val().role;
                    }).then(() => {*/
            res.render('listUsers.ejs', {
                data: JSON.stringify(resultado),
                name: user.displayName,
                role: role
            });
            //});
        });
});

router.get('/changePermission', function (req, res) {
    res.render('changePermission.ejs');
});

router.get("/listRoles", function (req, res) {
    var resultado = [];
    var util = firebase.database().ref('Roles');
    util.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            let input = {};
            input.Id = childSnapshot.key;
            input.Desc = childdata.Desc;
            resultado.push(input);
        });
    }).then(() => {
        res.json(resultado);
    });

});

router.get('/listUserOrderedFiltered', function (req, res) {
    //var user = firebase.auth().currentUser;
    var newResult;
    //var role;
    var order = req.query.order;
    var filter = req.query.filter;
    var resultado = [];
    var util = firebase.database().ref('users');
    if (filter === "" || filter === "todos") {
        util = util.orderByChild(normalizaPesquisa(order));
    } else {
        switch (filter.split("_")[0]) {
            case ("cargo"):
                util = util.orderByChild('role').equalTo(filter.split("_")[1]);
                break;
        }
    }
    util.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childdata = childSnapshot.val();
            let input = {};
            input.id = childSnapshot.key;
            input.isActive = childdata.isActive;
            input.name = childdata.username;
            input.role = childdata.role;
            input.age = childdata.bornDate;
            input.photo = (childdata.photo) ? ("data:image/png;base64," + childdata.photo) : "/images/avatar.png";
            resultado.push(input);
        });
    }).then(() => {
        newResult = removeIsInactive(resultado);

        if (filter != "" && order != "") {
            switch (order) {
                case ("Nome"):
                    resultado.sort((a, b) => a.name.localeCompare(b.name));
                    break;
                case ("Cargo"):
                    resultado.sort((a, b) => a.role.localeCompare(b.role));
                    break;
                case ("Data"):
                    resultado.sort((a, b) => new Date(a.age) - new Date(b.age));
                    break;
            }
        }

        var resp = "";


        if (resultado.length > 0) {
            var ejs = require('ejs');
            var path = require('path');
            ejs.renderFile(path.join(__dirname, '../public/views/users/listOfUsers.ejs'), {
                data: newResult
            }, function (err, html) {
                resp = html;
            });
        } else {
            resp = "<p style=\"color:red\">A pesquisa não retornou qualquer resultado</p>"
        }
        res.send(resp);
    });

})



router.post("/updateRole", function (req, res) {
    var json = {
        role: req.body.role
    }
    updateUserData(req.body.uid, json);
    res.send(JSON.stringify("Done"));
});

router.get("/changePassword", function (req, res) {
    var user = req.session.userObj;
    var role = req.session.role;
    var resultado = [];
    var helpRoot = firebase.database().ref("/help/changePassword");

    helpRoot.once('value').then(function (snapshot) {
        let input = {};
        input.currPassword = snapshot.val().currPassword;
        input.newPassword = snapshot.val().newPassword;
        input.newPasswordConfirm = snapshot.val().newPasswordConfirm;
        resultado.push(input);
    }).then(() =>{
        res.render('changePassword.ejs', {
            name: user.displayName,
            role: role,
            data: JSON.stringify(resultado)
        });
    });
});

function normalizaPesquisa(param) {
    var resposta = "";
    switch (param) {
        case "Nome":
            resposta = "username";
            break;
        case "Cargo":
            resposta = "role";
            break;
        case "Data":
            resposta = "bornDate";
            break;
        default:
            resposta = "";
    }
    return resposta;
}

function updateUserData(userId, json) {
    firebase.database().ref('users/' + userId).update(json);
}

function removeIsInactive(resultado) {
    var newResult = [];
    for (var i = 0, len = resultado.length; i < len; i++) {
        if (resultado[i].isActive === true) {
            newResult.push(resultado[i]);
        }
    }
    return newResult;
}

function calcAge(dateString) {
    var birthdate = new Date(dateString);
    var cur = new Date();
    var diff = cur - birthdate;
    var age = Math.floor(diff / 31557600000);
    return age;
}

module.exports = router;
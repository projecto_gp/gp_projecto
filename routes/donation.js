var express = require("express");
var router = express.Router();
var firebase = require('firebase');
const uuidv4 = require('uuid/v4');

router.get('/chooseDonation', async function (req, res) {
    var user = req.session.userObj;
    var role = req.session.role;
    var today = new Date();
    var date = today.getFullYear() + '-0' + (today.getMonth() + 2);
    res.render('chooseDonation.ejs', {
        name: user.displayName,
        role: role,
        minDate: date
    });
})

router.post('/chooseDonation', function (req, res) {
    var user = firebase.auth().currentUser;
    var role = req.session.role;
    var userId, donationType, dateTime, value, cardNumber, animalName;
    userId = user.uid;
    donationType = getDonationType(req.body.amount);

    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    dateTime = date + ' ' + time;
    value = parseInt(req.body.value);
    cardNumber = req.body.cardNumber;
    value = req.body.amount;
    animalName = (req.body.animalName) ? (req.body.animalName) : "";
    donationType = getDonationType(req.body.amount);

    var donations = firebase.database().ref('donations').orderByChild('userId').equalTo(userId);

    donations.once('value').then(function (snapshot) {
        if (snapshot.exists()) {
            snapshot.forEach(function (childSnapshot) {
                var childdata = childSnapshot.val();
                var json = {
                    value: parseInt(childdata.value) + parseInt(value)
                };
                console.log("update")
                updateDonationData(childSnapshot.key, json);
            });
        } else {
            console.log("write else")
            writeDonationData(userId, donationType, dateTime, parseInt(value), cardNumber, animalName)
        }
    }).then(() => {
        res.render('donationSuccess.ejs', {
            name: user.displayName,
            role: role
        });
    });
});



function getDonationType(amount) {
    if (amount >= 1 && amount < 10) {
        return "1";
    } else if (amount >= 10 && amount < 50) {
        return "2";
    } else if (amount >= 50) {
        return "3";
    }
}

function writeDonationData(userId, donationType, date, value, cardNumber, animalName) {
    var donationRef = firebase.database().ref('donations');
    var newDonationRef = donationRef.push();
    newDonationRef.set({
        userId: userId,
        donationType: donationType,
        animalName: animalName,
        cardNumber: cardNumber,
        date: date,
        value: value,
        assigned: false
    });
}
function updateUserData(userId, json) {
    firebase.database().ref('users/' + userId).update(json);
}

function updateDonationData(donationId, json) {
    firebase.database().ref('donations/' + donationId).update(json);
}

module.exports = router;
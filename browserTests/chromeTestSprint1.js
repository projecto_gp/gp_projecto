var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

var driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

function listUsers() {
    login();
    driver.get('http://localhost:8080/user/listUsers');
}

function seeProfile() {
    login();
    driver.get('http://localhost:8080/user/profile');
}

function editProfile() {
    login();
    driver.get('http://localhost:8080/user/profile');
    driver.findElement(By.id('editProfile')).click();
    driver.findElement(By.id('name')).clear();
    driver.findElement(By.id('name')).sendKeys('Antonio dos Santos');
    driver.findElement(By.id('email')).clear();
    driver.findElement(By.id('email')).sendKeys('seleniumChromeTest@gmail.com');
    driver.findElement(By.id('bornDate')).sendKeys('24101991');
    driver.findElement(By.id('address')).clear();
    driver.findElement(By.id('address')).sendKeys('v-block');
    driver.findElement(By.id('phoneNumber')).clear();
    driver.findElement(By.id('phoneNumber')).sendKeys('987456321');
    driver.findElement(By.id('password')).clear();
    driver.findElement(By.id('password')).sendKeys('memesmemes1');
    driver.findElement(By.id('editBtn')).click();
}

function register() {
    //driver.get('http://130.211.237.132:30006/auth/login');
    driver.get('http://localhost:8080/auth/register');
    driver.findElement(By.id('name')).sendKeys('seleniumChromeTest');
    driver.findElement(By.id('email')).sendKeys('seleniumChromeTest@gmail.com');
    driver.findElement(By.id('bornDate')).sendKeys('19111997');
    driver.findElement(By.id('address')).sendKeys('rua dos testes SC');
    driver.findElement(By.id('phoneNumber')).sendKeys('987456321');
    driver.findElement(By.id('password')).sendKeys('memesmemes1');
    driver.findElement(By.id('confpassword')).sendKeys('memesmemes1');
    driver.actions.moveToElement
    //var registWait = driver.wait(until.elementLocated(By.id('registerBtn')), 3000);
    var regist = driver.findElement(By.id('registerBtn'));
    driver.actions().moveToElement(regist).click().build().perform();
    //registWait.click();
    //driver.findElement(By.id('registerBtn')).click();
}

function login() {
    //driver.get('http://130.211.237.132:30006/auth/login');
    driver.get('http://localhost:8080/auth/login');
    driver.findElement(By.id('email')).sendKeys('testemundial14@gmail.com');
    driver.findElement(By.id('password')).sendKeys('memesmemes1');
    driver.findElement(By.id('loginBtn')).click();
}

register();

